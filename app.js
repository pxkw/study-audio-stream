var FRONT_PORT = 8080;
var BACK_PORT = 8081;

var express = require("express");
var BinaryServer = require('binaryjs').BinaryServer;
var fs = require('fs');
var wav = require('wav');

var app = express();
app.set('views', __dirname + '/tpl');
app.set('view engine', "jade");
app.engine('jade', require('jade').__express);
app.use(express.static(__dirname + '/public'))

app.get("/", function(req, res){
  res.render("index");
});

app.listen(FRONT_PORT);

binaryServer = BinaryServer({port: BACK_PORT});

binaryServer.on('connection', function(client) {
  console.log("Open connection.");

  var fileWriter = new wav.FileWriter('record/out.wav', {
    channels: 1,
    sampleRate: 48000,
    bitDepth: 16
  });

  client.on('stream', function(stream, meta) {
    console.log('Start receiving stream');
    stream.pipe(fileWriter);

    stream.on('end', function() {
      console.log('Finish receiving stream');
      fileWriter.end();
    });
  });
});
