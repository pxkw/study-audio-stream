function Player(navigator, AudioContext){
  var audioContext = null;

  var startRec = function(volProcessor, errProcessor) {
    console.log('Start recording.');

    var client = new BinaryClient('ws://localhost:8081');
    client.on('open', function() {
      window.Stream = client.createStream();
      navigator.getUserMedia(
        {audio:true},
        function(stream) {
          audioContext = new AudioContext();
          audioInput = audioContext.createMediaStreamSource(stream);
          recorder = audioContext.createScriptProcessor(2048, 1, 1);

          var analyser = audioContext.createAnalyser();
          analyser.smoothingTimeConstant = 0.3;
          analyser.fftSize = 2048;

          audioInput.connect(recorder)
          audioInput.connect(analyser);
          recorder.connect(audioContext.destination);

          recorder.onaudioprocess = function(event){
            console.log ('recording');
            if(volProcessor){
              var vol = byteFreqAverage(analyser);
              volProcessor(vol);
            }
            var cdata = event.inputBuffer.getChannelData(0);
            window.Stream.write(convertoFloat32ToInt16(cdata));
          }
        },
        function(reason) {
          alert('Error capturing audio.');
          console.error(reason.name);
          errProcessor && errProcessor();
        });
    });
  };

  var stopRec = function(cb){
    console.log('Stop recording.');
    if(!audioContext || audioContext.state=='closed'){
      console.log('Already stopped.')
    }else{
      audioContext.close().then(function(){
        window.Stream.end();
      });
    }
    cb && cb();
  };

  function convertoFloat32ToInt16(buffer) {
    var l = buffer.length;
    var buf = new Int16Array(l);
    while (l--) {
      buf[l] = buffer[l]*0xFFFF;    //convert to 16 bit
    }
    return buf.buffer
  }

  function byteFreqAverage(analyser){
    var byteFreq =  new Uint8Array(analyser.frequencyBinCount);
    analyser.getByteFrequencyData(byteFreq);
    return arrayAverage(byteFreq);
  }

  function arrayAverage(array){
    var sum = 0;
    var length = array.length;
    for (var i = 0; i < length; i++) {
      sum += array[i];
    }
    return (sum / length);
  }

  return {
    startRec: startRec,
    stopRec:  stopRec
  };
}
