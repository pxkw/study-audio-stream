function VolMeter(elemId){
  var LENGTH = 130;
  var HEIGHT = 10;
  var volMeter = document.getElementById(elemId).getContext('2d');
  volMeter.fillStyle = '#444444';
  var volClear = function(){
    volMeter.clearRect(0, 0, LENGTH, HEIGHT);
  };
  var volUpdate = function(vol){
    volClear();
    volMeter.fillRect(0,0,Math.min(vol,LENGTH),HEIGHT);
  }
  return {
    update : volUpdate,
    clear : volClear
  };
};
