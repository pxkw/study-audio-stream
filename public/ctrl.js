window.onload = function(){
  navigator.getUserMedia =
    navigator.webkitGetUserMedia ||
    navigator.getUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.oGetUserMedia ||
    navigator.msGetUserMedia;

  AudioContext =
    window.AudioContext ||
    window.webkitAudioContext  ||
    window.mozAudioContext ||
    window.oAudioContext ||
    window.msAudioContext;

  if(!navigator.getUserMedia || !AudioContext){
    alert('Not supported in this browser.');
  }

  var player = new Player(navigator, AudioContext);
  var volMeter = new VolMeter('vol-meter');
  var startRecBtn = document.getElementById('start-rec-btn');
  var stopRecBtn = document.getElementById('stop-rec-btn');

  startRecBtn.onclick = function(){
    disableBtn(startRecBtn);
    player.startRec(volMeter.update, stopRecBtn.onclick);
  };

  stopRecBtn.onclick = function(){
    player.stopRec(volMeter.clear);
    enableBtn(startRecBtn);
  };
};

var disableBtn = function(btn){
  btn.setAttribute('disabled','disabled');
};

var enableBtn = function(btn){
  btn.removeAttribute('disabled');
};
