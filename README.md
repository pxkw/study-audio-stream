# Quickstart

```
npm i
bower i
npm app.js
```

Then

  1. Access loclahost:8080
  1. Click "Start"
  1. Speak something a while
  1. Click "Stop"
  1. Open `record/out.wav`

# References

 + https://subvisual.co/blog/posts/39-tutorial-html-audio-capture-streaming-to-node-js-no-browser-extensions

# TODO

  1. Play an audio file stored in server
  1. List audio files stored in server

